/*
Part of the "chopchop" project at the University of Pennsylvania, USA.
Authors: Nik Sultana. 2019, 2020.

   Copyright 2021 University of Pennsylvania

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

#include "hello_interface.h"

enum security_options {SECOPT_NONE, SECOPT_POSIX_EX1, SECOPT_POSIX_EX2, SECOPT_SECCOMP};

#if defined(SECURE_NONE)
static const enum security_options security = SECOPT_NONE;
#include "secure_none.c"
#elif defined(SECURE_SECCOMP)
static const enum security_options security = SECOPT_SECCOMP;
#include "secure_seccomp.c"
#elif defined(SECURE_POSIX_EX2)
static const enum security_options security = SECOPT_POSIX_EX2;
#include "secure_posix_ex2.c"
#else
static const enum security_options security = SECOPT_POSIX_EX1;
#include "secure_posix_ex1.c"
#endif

static struct combin combins[NO_COMPARTS] =
  {{.path = "/home/nik/chopchop/compartmenting/hello_compartment/other"},
   {.path = "/home/nik/chopchop/compartmenting/hello_compartment/third"},
   {.path = "/home/nik/chopchop/compartmenting/hello_compartment/bye"}};
struct compart comparts[NO_COMPARTS] =
  {{.name = HC_NAME_K, .secure_fn = &secure_fn, .comms = NULL},
   {.name = OC_NAME_K, .secure_fn = &secure_fn, .comms = &combins[0]},
   {.name = TC_NAME_K, .secure_fn = &secure_fn, .comms = &combins[1]},
   {.name = BC_NAME_K, .secure_fn = &secure_fn, .comms = &combins[2]}};

struct extension_id *add_ten_ext = NULL;
struct extension_id *add_uid_ext = NULL;

#ifndef LC_ALLOW_EXCHANGE_FD
struct extension_data ext_add_int_to_arg(int num)
#else
struct extension_data ext_add_int_to_arg(int num, int fd)
#endif // ndef LC_ALLOW_EXCHANGE_FD
{
  struct extension_data result;
  result.bufc = sizeof(num);
  memcpy(result.buf, &num, sizeof(num));
#ifdef LC_ALLOW_EXCHANGE_FD
  result.fdc = 1;
  printf("(%d) ext_add_int_to_arg fd=%d\n", getpid(), fd);
  result.fd[0] = fd;
#endif // LC_ALLOW_EXCHANGE_FD
  return result;
}

#ifndef LC_ALLOW_EXCHANGE_FD
int ext_add_int_from_arg(struct extension_data data)
#else
int ext_add_int_from_arg(struct extension_data data, int *fd)
#endif // ndef LC_ALLOW_EXCHANGE_FD
{
  int result;
  memcpy(&result, data.buf, sizeof(result));
#ifdef LC_ALLOW_EXCHANGE_FD
  // FIXME assert result.fdc == 1;
  *fd = data.fd[0];
#endif // LC_ALLOW_EXCHANGE_FD
  return result;
}

static void add_ten_ORIG (int fd, int *num)
{
  dprintf(fd, "ext_add_ten() on %s. uid=%d\n", compart_name(), getuid());

  *num = *num + 10;
}

struct extension_data ext_add_ten(struct extension_data data)
{
  int fd = -1;
#ifndef LC_ALLOW_EXCHANGE_FD
  fd = STDOUT_FILENO;
  int num = ext_add_int_from_arg(data);
#else
  int num = ext_add_int_from_arg(data, &fd);
#endif // ndef LC_ALLOW_EXCHANGE_FD

  add_ten_ORIG(fd, &num);

#ifndef LC_ALLOW_EXCHANGE_FD
  return ext_add_int_to_arg(num);
#else
  return ext_add_int_to_arg(num, fd);
#endif // ndef LC_ALLOW_EXCHANGE_FD
}

void add_ten (int *fd, int *num)
{
#ifdef ADD_TEN_SHORTCIRCUIT
  assert(compart_shortcircuit());
  add_ten_ORIG(*fd, num);
#else
#ifndef LC_ALLOW_EXCHANGE_FD
  (void)fd;
  struct extension_data arg = ext_add_int_to_arg(*num);
  *num = ext_add_int_from_arg(compart_call_fn(add_ten_ext, arg));
#else
  struct extension_data arg = ext_add_int_to_arg(*num, *fd);
  *num = ext_add_int_from_arg(compart_call_fn(add_ten_ext, arg), fd);
#endif // ndef LC_ALLOW_EXCHANGE_FD
#endif // ADD_TEN_SHORTCIRCUIT
}

static void add_uid_ORIG (int fd, int *num)
{
  dprintf(fd, "ext_add_uid() on %s. uid=%d\n", compart_name(), getuid());

  *num = *num + getuid();
}

struct extension_data ext_add_uid(struct extension_data data)
{
  int fd = -1;
#ifndef LC_ALLOW_EXCHANGE_FD
  fd = STDOUT_FILENO;
  int num = ext_add_int_from_arg(data);
#else
  int num = ext_add_int_from_arg(data, &fd);
#endif // ndef LC_ALLOW_EXCHANGE_FD

  add_uid_ORIG(fd, &num);

#ifndef LC_ALLOW_EXCHANGE_FD
  return ext_add_int_to_arg(num);
#else
  return ext_add_int_to_arg(num, fd);
#endif // ndef LC_ALLOW_EXCHANGE_FD
}

void add_uid (int *fd, int *num)
{
#ifdef ADD_UID_SHORTCIRCUIT
  assert(compart_shortcircuit());
  add_uid_ORIG(*fd, num);
#else
#ifndef LC_ALLOW_EXCHANGE_FD
  (void)fd;
  struct extension_data arg = ext_add_int_to_arg(*num);
  *num = ext_add_int_from_arg(compart_call_fn(add_uid_ext, arg));
#else
  struct extension_data arg = ext_add_int_to_arg(*num, *fd);
  *num = ext_add_int_from_arg(compart_call_fn(add_uid_ext, arg), fd);
#endif // ndef LC_ALLOW_EXCHANGE_FD
#endif // ADD_UID_SHORTCIRCUIT
}

void hello_interface_check(void)
{
  if (compart_shortcircuit() && security != SECOPT_NONE) {
    fprintf(stderr, "This example must be built with -DSECURE_NONE since library was built with -DLC_SHORTCIRCUIT\n");
    exit(EXIT_FAILURE);
  }
}
