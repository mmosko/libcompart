/*
Part of the "chopchop" project at the University of Pennsylvania, USA.
Authors: Nik Sultana. 2019, 2020.

   Copyright 2021 University of Pennsylvania

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "bye.h"

struct thing {
  char data;
};

static struct thing *create_ORIG(void)
{
  return malloc(sizeof(struct thing));
}

static void destroy_ORIG(struct thing *th)
{
  free(th);
}

static void set_ORIG(struct thing *th, char x)
{
#if 0
  printf("  set_ORIG: (%p)%d: %d -", th, x, th->data);
#endif
  th->data = x;
#if 0
  printf("> %d\n", th->data);
#endif
}

static char get_ORIG(struct thing *th)
{
#if 0
  printf("  get_ORIG: (%p): %d\n", th, th->data);
#endif
  return th->data;
}

#include "bye_interface.h"
#include "bye_internal_interface.h"

static struct extension_data thing_arg_sender(struct thing *th)
{
  struct extension_data result;

  size_t offset = 0;
  size_t width = 0;

  offset += width;
  width = sizeof(th);
  memcpy(result.buf + offset, &th, width);

  result.bufc = offset + width;

#ifdef LC_ALLOW_EXCHANGE_FD
  result.fdc = 0;
#endif // LC_ALLOW_EXCHANGE_FD
  return result;
}

static void thing_arg_receiver(struct extension_data data, struct thing **result)
{
  size_t offset = 0;
  size_t width = 0;

  offset += width;
  width = sizeof(*result);
  memcpy(result, data.buf + offset, width);

  assert(offset + width == data.bufc);

#ifdef LC_ALLOW_EXCHANGE_FD
  assert(0 == data.fdc);
#endif // LC_ALLOW_EXCHANGE_FD
}

static struct extension_data set_arg_sender(struct thing *th, char x)
{
  struct extension_data result;

  size_t offset = 0;
  size_t width = 0;

  offset += width;
  width = sizeof(th);
  memcpy(result.buf + offset, &th, width);

  offset += width;
  width = sizeof(x);
  memcpy(result.buf + offset, &x, width);

  result.bufc = offset + width;

#ifdef LC_ALLOW_EXCHANGE_FD
  result.fdc = 0;
#endif // LC_ALLOW_EXCHANGE_FD

  return result;
}

static struct extension_data get_arg_sender(char x)
{
  struct extension_data result;

  size_t offset = 0;
  size_t width = 0;

  offset += width;
  width = sizeof(x);
  memcpy(result.buf + offset, &x, width);

  result.bufc = offset + width;

#ifdef LC_ALLOW_EXCHANGE_FD
  result.fdc = 0;
#endif // LC_ALLOW_EXCHANGE_FD

  return result;
}

static void set_arg_receiver(struct extension_data data, struct thing **th, char *x)
{
  size_t offset = 0;
  size_t width = 0;

  offset += width;
  width = sizeof(*th);
  memcpy(th, data.buf + offset, width);

  offset += width;
  width = sizeof(*x);
  memcpy(x, data.buf + offset, width);

  assert(offset + width == data.bufc);

#ifdef LC_ALLOW_EXCHANGE_FD
  assert(0 == data.fdc);
#endif // LC_ALLOW_EXCHANGE_FD
}

static void get_arg_receiver(struct extension_data data, char *x)
{
  size_t offset = 0;
  size_t width = 0;

  offset += width;
  width = sizeof(*x);
  memcpy(x, data.buf + offset, width);

  assert(offset + width == data.bufc);

#ifdef LC_ALLOW_EXCHANGE_FD
  assert(0 == data.fdc);
#endif // LC_ALLOW_EXCHANGE_FD
}

struct extension_data ext_create(struct extension_data data)
{
  (void)data;
  struct thing *th = create_ORIG(); // FIXME could check at this point if th==NULL
#if 0
  printf("[%d: Created %p]\n", getpid(), th);
#endif
  return thing_arg_sender(th);
}

struct extension_data ext_destroy(struct extension_data data)
{
  struct thing *th;
  thing_arg_receiver(data, &th);
#if 0
  printf("[%d: Destroying %p]\n", getpid(), th);
#endif
  destroy_ORIG(th);
  struct extension_data result; // empty
  result.bufc = 0;
#ifdef LC_ALLOW_EXCHANGE_FD
  result.fdc = 0;
#endif // LC_ALLOW_EXCHANGE_FD
  return result;
}

struct extension_data ext_get(struct extension_data data)
{
  struct thing *th;
  thing_arg_receiver(data, &th);
  char x = get_ORIG(th);
#if 0
  printf("%d: [Got %d from %p]\n", getpid(), x, th);
#endif
  return get_arg_sender(x);
}

struct extension_data ext_set(struct extension_data data)
{
  struct thing *th;
  char x;
  set_arg_receiver(data, &th, &x);
#if 0
  printf("[%d: Set %d in %p]\n", getpid(), x, th);
#endif
  set_ORIG(th, x);
#if 0
  char y = get_ORIG(th);
  printf("<Got %d from %p>\n", y, th);
#endif
  struct extension_data result; // empty
  result.bufc = 0;
#ifdef LC_ALLOW_EXCHANGE_FD
  result.fdc = 0;
#endif // LC_ALLOW_EXCHANGE_FD
  return result;
}

struct thing *create(void)
{
  struct extension_data arg;
  arg.bufc = 0;
#ifdef LC_ALLOW_EXCHANGE_FD
  arg.fdc = 0;
#endif // LC_ALLOW_EXCHANGE_FD
  struct thing *result = NULL;
  thing_arg_receiver(compart_call_fn(create_ext, arg), &result);
  return result;
}

void destroy(struct thing *th)
{
  struct extension_data arg = thing_arg_sender(th);
  compart_call_fn(destroy_ext, arg);
}

void set(struct thing *th, char x)
{
  struct extension_data arg = set_arg_sender(th, x);
  compart_call_fn(set_ext, arg);
}

char get(struct thing *th)
{
  struct extension_data arg = thing_arg_sender(th);
  char result;
  get_arg_receiver(compart_call_fn(get_ext, arg), &result);
  return result;
}

static struct thing *create_and_set_ORIG(char x)
{
  struct thing *th = create();
  set(th, x);
  return th;
}

static struct extension_data cas_arg_sender(char x)
{
  struct extension_data result;

  size_t offset = 0;
  size_t width = 0;

  offset += width;
  width = sizeof(x);
  memcpy(result.buf + offset, &x, width);

  result.bufc = offset + width;

#ifdef LC_ALLOW_EXCHANGE_FD
  result.fdc = 0;
#endif // LC_ALLOW_EXCHANGE_FD

  return result;
}

static void cas_arg_receiver(struct extension_data extdata, char *x)
{
  size_t offset = 0;
  size_t width = 0;

  offset += width;
  width = sizeof(*x);
  memcpy(x, extdata.buf + offset, width);

  assert(offset + width == extdata.bufc);

#ifdef LC_ALLOW_EXCHANGE_FD
  assert(0 == extdata.fdc);
#endif // LC_ALLOW_EXCHANGE_FD
}

struct extension_data ext_create_and_set(struct extension_data data)
{
  char x;
  cas_arg_receiver(data, &x);
  struct thing *th = create_and_set_ORIG(x);
  return thing_arg_sender(th);
}

struct thing *create_and_set(char x)
{
#if MANUAL_SHORT_CIRCUIT
  return create_and_set_ORIG(x);
#else
  struct extension_data arg = cas_arg_sender(x);
  struct thing *result = NULL;
  thing_arg_receiver(compart_call_fn(create_and_set_ext, arg), &result);
  return result;
#endif
}
