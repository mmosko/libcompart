/*
Part of the "chopchop" project at the University of Pennsylvania, USA.
Authors: Nik Sultana. 2019, 2020.

   Copyright 2021 University of Pennsylvania

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include <errno.h>
#include <seccomp.h>

void secure_fn (const char * const compartment_name)
{
  int rc = -1;
  int step = 0;
  scmp_filter_ctx ctx;

  ctx = seccomp_init(SCMP_ACT_LOG);
  step = 1;
  if (NULL == ctx) goto fail;

  rc = seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit), 0);
  step = 2;
  if (rc < 0) goto fail;

  rc = seccomp_load(ctx);
  step = 3;
  if (rc < 0) goto fail;

  return;

fail:
{
  char msg_buf[100];
  snprintf(msg_buf, MSG_BUF_SIZE - 1, "secure_fn() could not handle compartment '%s' at step %d: %s", compartment_name, step, strerror(errno));
  compart_log(msg_buf, strlen(msg_buf));;
  exit(1);
}

  // FIXME we don't apply seccomp_release(ctx);
}
