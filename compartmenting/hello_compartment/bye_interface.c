/*
Part of the "chopchop" project at the University of Pennsylvania, USA.
Authors: Nik Sultana. 2019, 2020.

   Copyright 2021 University of Pennsylvania

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include "bye_interface.h"
#include "bye_internal_interface.h"

#include <stdlib.h>

#include "bye.h"

struct extension_id *create_ext = NULL;
struct extension_id *destroy_ext = NULL;
struct extension_id *get_ext = NULL;
struct extension_id *set_ext = NULL;
struct extension_id *create_and_set_ext = NULL;

void lc_bye_register_fn(const char *compartment_name)
{
  create_ext = compart_register_fn(compartment_name, &ext_create);
  destroy_ext = compart_register_fn(compartment_name, &ext_destroy);
  set_ext = compart_register_fn(compartment_name, &ext_set);
  get_ext = compart_register_fn(compartment_name, &ext_get);
  create_and_set_ext = compart_register_fn(compartment_name, &ext_create_and_set);
}
