*FIXME* This output has not been updated for Allegheny.
*FIXME* Separate binaries noticed not working at 17b733c6e59df7b53b9986b060ee99de4ec2edf7, maybe due to changes in one of the recent versions.
```
$ make -C ../libcompart/ clean && make -C ../libcompart/
...
$ make clean && make
...
$ sudo ./hello_compartment
<42> initialising
<62> starting monitor 384073 (null)
<43> starting sub 384074 other compartment
<43> starting sub 384075 third compartment
<11> starting 384076 hello compartment
Old value: -5
<44> hello compartment to other compartment
<45> (monitor) call to other compartment
ext_add_ten() on other compartment. uid=65534
<46> (monitor) return from other compartment
<44> hello compartment terminating other compartment
<74> (monitor) notified of compart_ending other compartment
<44> hello compartment to third compartment
<73> instructed to terminate: other compartment
<45> (monitor) call to third compartment
ext_add_uid() on third compartment. uid=0
<46> (monitor) return from third compartment
After adding 10+0 to old value
New value: 5
<76> compart_ended: other compartment
<38> communication break: between (monitor) and hello compartment
<50> terminated: hello compartment
<50> terminated: third compartment
<37> all children dead
```

To use seccomp:
```
$ make clean && CFLAGS="-DSECURE_SECCOMP -lseccomp" make
...
$ ./hello_compartment
<42> initialising
<62> starting monitor 384139 (null)
<43> starting sub 384141 third compartment
<43> starting sub 384140 other compartment
<11> starting 384142 hello compartment
Old value: -5
<44> hello compartment to other compartment
<45> (monitor) call to other compartment
ext_add_ten() on other compartment. uid=1001
<46> (monitor) return from other compartment
<44> hello compartment terminating other compartment
<74> (monitor) notified of compart_ending other compartment
<44> hello compartment to third compartment
<73> instructed to terminate: other compartment
<45> (monitor) call to third compartment
ext_add_uid() on third compartment. uid=1001
<76> compart_ended: other compartment
<46> (monitor) return from third compartment
After adding 10+0 to old value
New value: 1006
<50> terminated: hello compartment
<38> communication break: between (monitor) and hello compartment
<50> terminated: third compartment
<37> all children dead
```
