/*
Part of the "chopchop" project at the University of Pennsylvania, USA.
Authors: Nik Sultana. 2019, 2020.

   Copyright 2021 University of Pennsylvania

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
extern struct extension_id *create_ext;
extern struct extension_id *destroy_ext;
extern struct extension_id *get_ext;
extern struct extension_id *set_ext;
extern struct extension_id *create_and_set_ext;

struct extension_data ext_create(struct extension_data data);
struct extension_data ext_destroy(struct extension_data data);
struct extension_data ext_get(struct extension_data data);
struct extension_data ext_set(struct extension_data data);
struct extension_data ext_create_and_set(struct extension_data data);
