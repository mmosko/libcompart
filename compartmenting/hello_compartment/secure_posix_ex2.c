/*
Part of the "chopchop" project at the University of Pennsylvania, USA.
Authors: Nik Sultana. 2019, 2020.

   Copyright 2021 University of Pennsylvania

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include <errno.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>

void secure_fn (const char * const compartment_name)
{
  struct rlimit rlim = {.rlim_cur = 1, .rlim_max = RLIM_INFINITY};

  if (0 != setrlimit(RLIMIT_NPROC, &rlim)) {
    char msg_buf[100];
    snprintf(msg_buf, MSG_BUF_SIZE - 1, "secure_fn() could not set rlimit: %s", strerror(errno));
    compart_log(msg_buf, strlen(msg_buf));;
    exit(1);
  }

  if (0 == strcmp(compartment_name, HC_NAME_K)) {
    compart_drop_privs(65534, 65534, "/tmp");
  } else if (0 == strcmp(compartment_name, OC_NAME_K)) {
    compart_drop_privs(65534, 65534, "/tmp");
  } else if (0 == strcmp(compartment_name, BC_NAME_K)) {
    compart_drop_privs(65534, 65534, "/tmp");
  } else if (0 == strcmp(compartment_name, TC_NAME_K)) {
    compart_drop_privs(0, 0, "/tmp");
  } else {
    char msg_buf[100];
    snprintf(msg_buf, MSG_BUF_SIZE - 1, "secure_fn() could not handle compartment: %s", compartment_name);
    compart_log(msg_buf, strlen(msg_buf));;
    exit(1);
  }
}
