/*
Part of the "chopchop" project at the University of Pennsylvania, USA.
Authors: Nik Sultana. 2019, 2020.

   Copyright 2021 University of Pennsylvania

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "bye.h"

#include "hello_interface.h"
#include "bye_interface.h"

static void bye_compound_call_test(int fd, const int num_things, struct thing* things[])
{
  dprintf(fd, "Starting compound call test\n");
  const int first_thing = 0;
  things[first_thing] = create_and_set((char)num_things);
  dprintf(fd, "Stored (%p) vs Expected value: %d vs %d\n", things[first_thing], get(things[first_thing]), (char)num_things);
  destroy(things[first_thing]);
  dprintf(fd, "Ended compound call test\n");
}

static void bye_test(int fd, const int num_things, const char initial_value, struct thing* things[])
{
  dprintf(fd, "Initial value for next test: %d\n", initial_value);

  for (int i = 0; i < num_things; i++) {
    things[i] = create();
  }

  for (int i = 0; i < num_things; i++) {
    set(things[i], initial_value + i);
    dprintf(fd, "Stored (%p) %d\n", things[i], initial_value + i);
  }

  for (int i = 0; i < num_things; i++) {
    dprintf(fd, "Stored (%p) vs Expected value: %d vs %d\n", things[i], get(things[i]), initial_value + i);
  }

  for (int i = 0; i < num_things; i++) {
    destroy(things[i]);
  }
}

int main(int argc, char **argv) {
  (void)argc; // These are to pacify warnings about unused variables.
  (void)argv;

  hello_interface_check();

  compart_check();
  compart_init(NO_COMPARTS, comparts, default_config);
  add_ten_ext = compart_register_fn(OC_NAME_K, &ext_add_ten);
  add_uid_ext = compart_register_fn(TC_NAME_K, &ext_add_uid);
  lc_bye_register_fn(BC_NAME_K);
  compart_start(HC_NAME_K);

#ifndef LC_ALLOW_EXCHANGE_FD
  int fd = STDOUT_FILENO;
#else
  const char *filename = "stdout";
  int fd = open(filename, O_CREAT|O_WRONLY|O_TRUNC, S_IRUSR|S_IWUSR);
  printf("(%d) open '%s' fd=%d\n", getpid(), filename, fd);
#endif // ndef LC_ALLOW_EXCHANGE_FD

  int original_value = -5;
  dprintf(fd, "Old value: %d\n", original_value);

  int new_value = original_value;
  add_ten(&fd, &new_value);

  compart_end(OC_NAME_K);

  add_uid(&fd, &new_value);

  dprintf(fd, "After adding 10+uid to old value\n");
  dprintf(fd, "New value: %d\n", new_value);

  const char initial_value = (char)new_value;
  const int num_things = 3;
  struct thing* things[num_things];
  bye_test(fd, num_things, initial_value, things);
  bye_compound_call_test(fd, num_things, things);

  close(fd);
  return EXIT_SUCCESS;
}
