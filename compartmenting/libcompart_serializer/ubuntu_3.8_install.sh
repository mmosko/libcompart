#!/bin/bash
set -e

sudo apt install clang
sudo apt install llvm-toolchain
pip install https://pypi.python.org/packages/source/c/clang/clang-3.8.tar.gz --user
pip install pyyaml
