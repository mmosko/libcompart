# Part of the "chopchop" project at the University of Pennsylvania, USA.
# Authors: Henry Zhu. 2019.
#
#    Copyright 2021 University of Pennsylvania
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

# special generation code for special data types
import os
import re
import yaml

class DataStructure():
    def __init__(self, code, base_replacements, replacements):
        self.code = code
        self.base_replacements = base_replacements
        self.replacements_list = replacements
        self.replacements = {r: None for r in replacements}
   
    def get_replacements(self):
        return self.replacements_list

    def fill_replacement(self, replacement, answer):
        self.replacements[replacement] = answer

    def get_resolved_code(self, base_type, member_name):
        result_code = self.code[:]
        for r, ans in self.base_replacements.items():
            if ans == 'BASE_TYPE':
	        result_code = re.sub(r, base_type, result_code)
            elif ans == 'MEMBER_NAME':
                result_code = re.sub(r, member_name, result_code)
        for r, ans in self.replacements.items():
            result_code = re.sub(r, ans, result_code)

        print(result_code)
        return result_code

def get_data_structures(file_path):
    data_structures = {}
    files = [f for f in os.listdir(file_path) if os.path.isfile(os.path.join(file_path, f))]
    for filename in files:
        base_name, extension = os.path.splitext(filename)
        if extension != '.yaml':
            continue
        path = os.path.join(file_path, filename)
        with open(path, 'r') as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
            replacements = data['replacements']
            base_replacements = data['base_replacements']
            code = data['code']
            data_structures[base_name] = DataStructure(code, base_replacements, replacements)

    return data_structures

singular_linked_list = '''
%NODE_DATA_TYPE%* node = TEST_data->next; //MARSHALL_ONLY
%NODE_DATA_TYPE%* node = TEST_data; //UNMARSHALL_ONLY

size_t linked_list_length = 0;
%NODE_DATA_TYPE%* counter_node = node; //MARSHALL_ONLY
while(counter_node != NULL) //MARSHALL_ONLY
{ //MARSHALL_ONLY
	counter_node = counter_node->%NEXT_NODE%; //MARSHALL_ONLY
	linked_list_length++; //MARSHALL_ONLY
} //MARSHALL_ONLY
marshall_prim(buf, &buf_index, linked_list_length); //MARSHALL_ONLY
while(node != NULL) //MARSHALL_ONLY
{ //MARSHALL_ONLY
	%MARSHALL(node)% //MARSHALL_ONLY
	node = node->%NEXT_NODE%; //MARSHALL_ONLY
} //MARSHALL_ONLY

marshall_prim(buf, &buf_index, linked_list_length); //UNMARSHALL_ONLY
for(size_t i = 0; i < linked_list_length; i++) //UNMARSHALL_ONLY
{ //UNMARSHALL_ONLY
	%NODE_DATA_TYPE%* n_node; //UNMARSHALL_ONLY
	%MARSHALL(n_node)% //UNMARSHALL_ONLY
	node->%NEXT_NODE% = n_node; //UNMARSHALL_ONLY
	n_node->%NEXT_NODE% = NULL; //UNMARSHALL_ONLY
	node = n_node; //UNMARSHALL_ONLY
} //UNMARSHALL_ONLY
'''
circular_linked_list = '''
%NODE_DATA_TYPE%* head = TEST_data;

size_t linked_list_length = 0;
%NODE_DATA_TYPE%* counter_node = head->%NEXT_NODE%; //MARSHALL_ONLY
while(counter_node != head) //MARSHALL_ONLY
{ //MARSHALL_ONLY
	linked_list_length++; //MARSHALL_ONLY
	counter_node = counter_node->%NEXT_NODE%; //MARSHALL_ONLY
} //MARSHALL_ONLY
marshall_prim(buf, &buf_index, linked_list_length); //MARSHALL_ONLY
%NODE_DATA_TYPE%* node = head->next; //MARSHALL_ONLY
while(node != head) //MARSHALL_ONLY
{ //MARSHALL_ONLY
	%MARSHALL(node)% //MARSHALL_ONLY
	node = node->%NEXT_NODE%; //MARSHALL_ONLY
} //MARSHALL_ONLY

%NODE_DATA_TYPE%* node = head; //UNMARSHALL_ONLY
marshall_prim(buf, &buf_index, linked_list_length); //UNMARSHALL_ONLY
for(size_t i = 0; i < linked_list_length; i++) //UNMARSHALL_ONLY
{ //UNMARSHALL_ONLY
	%NODE_DATA_TYPE%* n_node; //UNMARSHALL_ONLY
	%MARSHALL(n_node)% //UNMARSHALL_ONLY
	node->%NEXT_NODE% = n_node; //UNMARSHALL_ONLY
	n_node->%PREV_NODE% = node; //UNMARSHALL_ONLY
	n_node->%NEXT_NODE% = head; //UNMARSHALL_ONLY
	node = n_node; //UNMARSHALL_ONLY
} //UNMARSHALL_ONLY
'''

# NOTE:
# The doubly linked list is a lot like the singular linked list, in particular,
# their marshalling code is identical, since we don't need backward references
# when marshalling things.
# The unamrshalling code is different, only in unmarshalling the *prev pointer,
# during which we assign the *prev to the previously allocated node.

doubly_linked_list = '''
%NODE_DATA_TYPE%* node = TEST_data->next; //MARSHALL_ONLY
%NODE_DATA_TYPE%* node = TEST_data; //UNMARSHALL_ONLY

size_t linked_list_length = 0;
%NODE_DATA_TYPE%* counter_node = node; //MARSHALL_ONLY
while(counter_node != NULL) //MARSHALL_ONLY
{ //MARSHALL_ONLY
	counter_node = counter_node->%NEXT_NODE%; //MARSHALL_ONLY
	linked_list_length++; //MARSHALL_ONLY
} //MARSHALL_ONLY
marshall_prim(buf, &buf_index, linked_list_length); //MARSHALL_ONLY
while(node != NULL) //MARSHALL_ONLY
{ //MARSHALL_ONLY
	%MARSHALL(node)% //MARSHALL_ONLY
	node = node->%NEXT_NODE%; //MARSHALL_ONLY
} //MARSHALL_ONLY

marshall_prim(buf, &buf_index, linked_list_length); //UNMARSHALL_ONLY
for(size_t i = 0; i < linked_list_length; i++) //UNMARSHALL_ONLY
{ //UNMARSHALL_ONLY
	%NODE_DATA_TYPE%* n_node; //UNMARSHALL_ONLY
	%MARSHALL(n_node)% //UNMARSHALL_ONLY
	node->%NEXT_NODE% = n_node; //UNMARSHALL_ONLY
    n_node->%PREV_NODE% = node; //UNMARSHALL_ONLY
	n_node->%NEXT_NODE% = NULL; //UNMARSHALL_ONLY
	node = n_node; //UNMARSHALL_ONLY
} //UNMARSHALL_ONLY
'''
binary_tree = '''
%BEGIN_FUNCTION%
void serialize_binary_tree(%NODE_DATA_TYPE%* node, int* valids, int* values, int* values_index)
{
	if(node != NULL)
	{
		valids[*values_index] = 1;
		values[*values_index] = node->val;
		(*values_index)++;
		serialize_binary_tree(node->%LEFT_NODE%, valids, values, values_index);
		serialize_binary_tree(node->%RIGHT_NODE%, valids, values, values_index);
	}
	else
	{
		valids[*values_index] = 0;
		(*values_index)++;
	}
}
%END_FUNCTION%
%BEGIN_FUNCTION%
%NODE_DATA_TYPE%* deserialize_binary_tree(int* valids, int* values, int* values_index)
{
	%NODE_DATA_TYPE%* node = calloc(sizeof(%NODE_DATA_TYPE%), 1);
	int valid = valids[*values_index];
	int value = values[*values_index];
	(*values_index)++;
	if(!valid) {
		return NULL;
	}
	node->val = value;
	node->%LEFT_NODE% = deserialize_binary_tree(valids, values, values_index);
	node->%RIGHT_NODE% = deserialize_binary_tree(valids, values, values_index);
	return node;
}
%END_FUNCTION%

int* valids = calloc(sizeof(int), 1000); //MARSHALL_ONLY
int* values = calloc(sizeof(int), 1000); //MARSHALL_ONLY
int values_index = 0; //MARSHALL_ONLY
int* values_index_ptr = &values_index; //MARSHALL_ONLY
serialize_binary_tree(TEST_data, valids, values, values_index_ptr); //MARSHALL_ONLY
marshall_prim(buf, &buf_index, values_index); //MARSHALL_ONLY
marshall_size(buf, &buf_index, valids, values_index); //MARSHALL_ONLY
marshall_size(buf, &buf_index, values, values_index); //MARSHALL_ONLY

int values_index = 0; //UNMARSHALL_ONLY
marshall_prim(buf, &buf_index, values_index); //UNMARSHALL_ONLY
int* valids = calloc(sizeof(int), values_index); //UNMARSHALL_ONLY
int* values = calloc(sizeof(int), values_index); //UNMARSHALL_ONLY
marshall_size(buf, &buf_index, valids, values_index); //UNMARSHALL_ONLY
marshall_size(buf, &buf_index, values, values_index); //UNMARSHALL_ONLY
values_index = 0; //UNMARSHALL_ONLY
int* values_index_ptr = &values_index; //UNMARSHALL_ONLY
%NODE_DATA_TYPE%* root = deserialize_binary_tree(valids, values, values_index_ptr); //UNMARSHALL_ONLY
memcpy(TEST_data, root, sizeof(%NODE_DATA_TYPE%)); //UNMARSHALL_ONLY
'''

data_structures = {
    'singular_linked_list': singular_linked_list,
    'circular_linked_list': circular_linked_list,
    'doubly_linked_list': doubly_linked_list,
    'binary_tree': binary_tree,
}
