// RUN: printf "TestStructWithPointerToPrimitiveStruct\n" | python -B setup.py struct_interface.h
// RUN: %CC %C_FLAGS -I../headers %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG=%t.log %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "structs.h"
#include "compart_api.h"
#include "struct_interface.h"

int main()
{
  	compart_init(NO_COMPARTS, comparts, default_config);

	struct extension_id* struct_ext = compart_register_fn("other compartment", &ext_TestStructWithPointerToPrimitiveStruct);

  	compart_start("struct compartment");

	struct StructWithPointerToPrimitiveStruct* struct_with_pointer_to_struct = calloc(sizeof(*struct_with_pointer_to_struct), 1);
	struct_with_pointer_to_struct->primitive_struct = (struct PrimitiveStruct*) calloc(sizeof(struct PrimitiveStruct), 1);

	struct_with_pointer_to_struct->integer = INITIAL_NUMBER;
	
	//struct_with_pointer_to_struct->primitive_struct = (struct PrimitiveStruct) malloc(sizeof(struct PrimitiveStruct));
	struct_with_pointer_to_struct->primitive_struct->integer = INITIAL_NUMBER;
	struct_with_pointer_to_struct->primitive_struct->character = INITIAL_CHARACTER;

	struct extension_data arg = ext_TestStructWithPointerToPrimitiveStruct_to_arg(struct_with_pointer_to_struct);
	int result = ext_TestStructWithPointerToPrimitiveStruct_from_resp(compart_call_fn(struct_ext, arg));
	if(result > 0)
	{
		printf("VULNERABLE, GOT A RESULT OF %d\n", result);
	}
	else
	{
		printf("NOT VULNERABLE\n");
// CHECK: NOT VULNERABLE
	}

	fflush(stdout);
	free(struct_with_pointer_to_struct);
	ext_TestStructWithPointerToPrimitiveStruct_from_resp(compart_call_fn(struct_ext, arg));
	return 0;
}
