// RUN: printf "TestTypedefPrimitiveStruct\n" | python -B setup.py struct_interface.h
// RUN: %CC %C_FLAGS -I../headers %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG=%t.log %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "structs.h"
#include "compart_api.h"
#include "struct_interface.h"

int main()
{
  	compart_init(NO_COMPARTS, comparts, default_config);

	struct extension_id* struct_ext = compart_register_fn("other compartment", &ext_TestTypedefPrimitiveStruct);

  	compart_start("struct compartment");

	TypedefPrimitiveStruct* td_primitive_struct = calloc(sizeof(*td_primitive_struct), 1);
	td_primitive_struct->integer = INITIAL_NUMBER;
	td_primitive_struct->character = INITIAL_CHARACTER;

	struct extension_data arg = ext_TestTypedefPrimitiveStruct_to_arg(td_primitive_struct);
	int result = ext_TestTypedefPrimitiveStruct_from_resp(compart_call_fn(struct_ext, arg));
	if(result > 0)
	{
		printf("VULNERABLE, GOT A RESULT OF %d\n", result);
	}
	else
	{
		printf("NOT VULNERABLE\n");
// CHECK: NOT VULNERABLE
	}
	
	fflush(stdout);
	free(td_primitive_struct);
	ext_TestTypedefPrimitiveStruct_from_resp(compart_call_fn(struct_ext, arg));
	return 0;
}
