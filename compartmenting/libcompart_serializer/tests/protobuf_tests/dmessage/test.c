// RUN: protoc-c --proto_path=%S --c_out=. dmessage.proto 
// RUN: mv %S/dmessage.pb-c.c %S/dmessage.pb-c.cc

// RUN: printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" | python -B setup.py dmessage_interface.h
// RUN: sed -i '/data_ref->data_ref/d' dmessage_interface.h
// RUN: %CC %C_FLAGS -I%S %s %S/dmessage.pb-c.cc -lprotobuf-c -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG=%t.log %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "compart_api.h"
#include "dmessage_interface.h"

int main()
{
    compart_init(NO_COMPARTS, comparts, default_config);
  	return_same_ext = compart_register_fn("other compartment", &ext_return_same);

  	compart_start("hello compartment");

    DMessage msg = DMESSAGE__INIT;
    msg.d = some_message;

	struct extension_data arg = ext_return_same_to_arg(&msg);
	int success = ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));

	printf("success: %d\n", success);
// CHECK: success: 1

	fflush(stdout);
	ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));
	return 0;
}

