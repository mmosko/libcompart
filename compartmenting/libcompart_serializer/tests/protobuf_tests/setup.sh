#!/bin/bash

cd /tmp
wget https://github.com/protocolbuffers/protobuf/releases/download/v3.11.4/protobuf-cpp-3.11.4.zip
unzip -qq -o protobuf-cpp-3.11.4.zip
cd protobuf-3.11.4
./configure && make && make install
cd ..
wget https://github.com/protobuf-c/protobuf-c/releases/download/v1.3.3/protobuf-c-1.3.3.tar.gz
tar xvf protobuf-c-1.3.3.tar.gz
cd protobuf-c-1.3.3
./configure && make && make install

