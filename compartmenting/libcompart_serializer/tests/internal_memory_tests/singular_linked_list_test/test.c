// RUN: cat input.txt | python -B setup.py singular_linked_list_interface.h
// RUN: %CC %C_FLAGS -I../headers %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG="%t.log" valgrind --leak-check=full %t 2>&1 || true; } | %FileCheck %s
// ADD TURN RN to RUN TO SEE RESULTS TO A FILE
// RN: sudo PITCHFORK_LOG="%t.log" valgrind --leak-check=full %t &> %t.valgrind

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "structs.h"
#include "compart_api.h"
#include "singular_linked_list_interface.h"

int main()
{
    struct Node *head = calloc(1, sizeof(struct Node));
    head->val = 1;
    head->next = NULL;
    struct Node *tmp = head;

    for (int i = 2; i < 4; i++)
    {
        struct Node *n_node = calloc(1, sizeof(struct Node));
        n_node->val = i;
        n_node->next = NULL;
        tmp->next = n_node;
        tmp = tmp->next;
    }

    struct extension_data arg = ext_TestLinkedList_to_arg(head);
    int result = ext_TestLinkedList_from_resp(ext_TestLinkedList(arg));
    if (result > 0)
    {
        printf("VULNERABLE, GOT A RESULT OF %d\n", result);
    }
    else
    {
        printf("NOT VULNERABLE\n");
        // CHECK: NOT VULNERABLE
        // CHECK-NOT: definitely lost
        // CHECK-NOT: LEAK SUMMARY
    }

    fflush(stdout);
    clean_up(head);
    return 0;
}
