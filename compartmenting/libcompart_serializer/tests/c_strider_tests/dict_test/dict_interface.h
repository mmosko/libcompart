
#include "return_same.h"
#include <string.h>
#include <stdlib.h>

#define NO_COMPARTS 2

static struct compart comparts[NO_COMPARTS] =
  {{.name = "struct compartment", .uid = 65534, .gid = 65534, .path = "/dev/"},
   {.name = "other compartment", .uid = 65534, .gid = 65534, .path = "/dev/"}};

struct extension_id *return_same_ext = NULL;

int ext_return_same_from_resp(struct extension_data data);

void _unmarshall_dictType(char* buf, size_t* buf_index_, dictType** TEST_data);

void marshall_dictht(char* buf, size_t* buf_index_, dictht* TEST_data);

struct extension_data ext_return_same(struct extension_data data);

struct extension_data ext_return_same_to_arg(dict* d);

void marshall_dictType(char* buf, size_t* buf_index_, dictType* TEST_data);

struct extension_data ext_return_same_to_resp(int result);

void _unmarshall_dict(char* buf, size_t* buf_index_, dict** TEST_data);

void _unmarshall_dictht(char* buf, size_t* buf_index_, dictht** TEST_data);

void marshall_dict(char* buf, size_t* buf_index_, dict* TEST_data);

void ext_return_same_from_arg(struct extension_data data, dict* *d);

#define unmarshall_dictType(a, b, c) 	_unmarshall_dictType(a, b, &c)

#define unmarshall_dict(a, b, c) 	_unmarshall_dict(a, b, &c)

#define unmarshall_dictht(a, b, c) 	_unmarshall_dictht(a, b, &c)

#ifndef _LIBCOMPART_SERIALISATION__
#define _LIBCOMPART_SERIALISATION__

void unmarshall_string_(char* buf, size_t* buf_index_, char** str);
void marshall_string(char* buf, size_t* buf_index_, char* str);

#define MARSHALL_CAT_(a, b)   a##b

#define MARSHALL_CAT(a, b)   MARSHALL_CAT_(a, b)

#define unmarshall_string(buf, buf_index, str)   unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_prim(buf, buf_index, data)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data);   memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define marshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   memcpy(&buf[*(buf_index)], data, MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_size(buf, buf_index, data, size)   size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size;   data = calloc(size, sizeof(data[0]));   memcpy(data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__));   *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#endif // _LIBCOMPART_SERIALISATION__


//marshall string to [size][str]
void marshall_string(char* buf, size_t* buf_index_, char* str)
{
  size_t buf_index = *buf_index_;

  if(str)
  {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  }
  else
  {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }

  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str)
{
  *str = NULL;
  size_t buf_index = *buf_index_;

  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);

  if(str_length > 0)
  {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }

  *buf_index_ = buf_index;
}
void _unmarshall_dictType(char* buf, size_t* buf_index_, dictType** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    dictType* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->hashFunction);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->keyDup);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->valDup);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->keyCompare);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->keyDestructor);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->valDestructor);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_dictType(a, b, c) 	_unmarshall_dictType(a, b, &c)

void marshall_dictht(char* buf, size_t* buf_index_, dictht* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->size);
    marshall_prim(buf, &buf_index, TEST_data->sizemask);
    marshall_prim(buf, &buf_index, TEST_data->used);
    for (size_t marshall_index1 = 0; marshall_index1 < TEST_data->used; marshall_index1++)
    {
      dictEntry * table1 = TEST_data->table[marshall_index1];
      size_t num_elements = 0;
        fprintf(stderr, "NUM_ITEMS %d %d\n", TEST_data->size, TEST_data->used); 
        fprintf(stderr, "MAS BEGIn %d\n", buf_index); 
        marshall_prim(buf, &buf_index, TEST_data->size); 
        for(size_t i = 1; i < TEST_data->size; i++) 
        { 
          dictEntry* node = TEST_data->table[i]; 
          while(node != NULL) 
          { 
              num_elements++; 
              node = node->next; 
          } 
        } 
        marshall_prim(buf, &buf_index, num_elements); 
        fprintf(stderr, "NUM_ITEMS %d\n", num_elements); 
        for(size_t i = 1; i < TEST_data->size; i++) 
        { 
          dictEntry* node = TEST_data->table[i]; 
          while(node != NULL) 
          { 
            marshall_prim(buf, &buf_index, i); 
            fprintf(stderr, "ITEM %d %d\n", i, node->key); 
            marshall_size(buf, &buf_index, node, 1); 
       
            node = node->next; 
          } 
        } 
        fprintf(stderr, "MAS END %d\n", buf_index); 
        break; 
    }
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void marshall_dictType(char* buf, size_t* buf_index_, dictType* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_prim(buf, &buf_index, TEST_data->hashFunction);
    marshall_prim(buf, &buf_index, TEST_data->keyDup);
    marshall_prim(buf, &buf_index, TEST_data->valDup);
    marshall_prim(buf, &buf_index, TEST_data->keyCompare);
    marshall_prim(buf, &buf_index, TEST_data->keyDestructor);
    marshall_prim(buf, &buf_index, TEST_data->valDestructor);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
void _unmarshall_dict(char* buf, size_t* buf_index_, dict** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    dict* TEST_data_ref = *TEST_data;
    unmarshall_dictType(buf, &buf_index, TEST_data_ref->type);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->privdata);
    for (size_t unmarshall_flat_index0 = 0; unmarshall_flat_index0 < 2; unmarshall_flat_index0++)
    {
      struct dictht * ht1 = &TEST_data_ref->ht[unmarshall_flat_index0];
      unmarshall_dictht(buf, &buf_index, ht1);
      memcpy(&TEST_data_ref->ht[unmarshall_flat_index0], ht1, sizeof(*ht1));
    }
    unmarshall_prim(buf, &buf_index, TEST_data_ref->rehashidx);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->iterators);
  }
  *buf_index_ = buf_index;
}

#define unmarshall_dict(a, b, c) 	_unmarshall_dict(a, b, &c)

void _unmarshall_dictht(char* buf, size_t* buf_index_, dictht** TEST_data)
{
  size_t size_of_element_= 0;
  size_t buf_index = *buf_index_;
  unmarshall_prim(buf, &buf_index, size_of_element_);
  *TEST_data = NULL;
  if(size_of_element_)
  {
    *TEST_data = calloc(1, sizeof(**TEST_data));
    dictht* TEST_data_ref = *TEST_data;
    unmarshall_prim(buf, &buf_index, TEST_data_ref->size);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->sizemask);
    unmarshall_prim(buf, &buf_index, TEST_data_ref->used);
    TEST_data_ref->table = calloc(TEST_data_ref->used, sizeof(*TEST_data_ref->table));
    for (size_t unmarshall_index1 = 0; unmarshall_index1 < TEST_data_ref->used; unmarshall_index1++)
    {
      TEST_data_ref->table[unmarshall_index1] = calloc(1, sizeof(*TEST_data_ref->table[unmarshall_index1]));
      dictEntry * table1 = TEST_data_ref->table[unmarshall_index1];
      size_t num_elements = 0;
        fprintf(stderr, "UNMAS BEGIn %d\n", buf_index); 
        size_t table_size = 0; 
        unmarshall_prim(buf, &buf_index, table_size); 
         
        free(TEST_data_ref->table); 
        TEST_data_ref->table = calloc(table_size, sizeof(*TEST_data_ref->table)); 
        
        fprintf(stderr, "UNTABLE %d\n", table_size); 
         
        unmarshall_prim(buf, &buf_index, num_elements); 
        fprintf(stderr, "NUM_ITEMS %d\n", num_elements); 
       
        for(size_t unused = 0; unused < num_elements; unused++)  
        { 
          size_t i = 0; 
          unmarshall_prim(buf, &buf_index, i); 
       
          dictEntry* new_node = NULL; 
          unmarshall_size(buf, &buf_index, new_node, 1); 
           
          fprintf(stderr, "ITEM %d %d\n", i, new_node->key); 
       
          dictEntry* node = TEST_data_ref->table[i]; 
          fprintf(stderr, "HEY %p\n", node); 
          if(node == NULL) 
          { 
          fprintf(stderr, "HEY2\n"); 
              TEST_data_ref->table[i] = new_node; 
          } 
          else 
          { 
          fprintf(stderr, "HEY3\n"); 
              while(node->next != NULL) 
              { 
                  node = node->next; 
              } 
              node->next = new_node; 
          } 
        } 
        fprintf(stderr, "UNMAS END %d\n", buf_index); 
        break; 
        
    }
  }
  *buf_index_ = buf_index;
}

#define unmarshall_dictht(a, b, c) 	_unmarshall_dictht(a, b, &c)

void marshall_dict(char* buf, size_t* buf_index_, dict* TEST_data)
{
  size_t buf_index = *buf_index_;
  if(TEST_data)
  {
    size_t size_of_element_= sizeof(*TEST_data);
    marshall_prim(buf, &buf_index, size_of_element_);
    marshall_dictType(buf, &buf_index, TEST_data->type);
    marshall_prim(buf, &buf_index, TEST_data->privdata);
    for (size_t marshall_flat_index0 = 0; marshall_flat_index0 < 2; marshall_flat_index0++)
    {
      struct dictht * ht1 = &TEST_data->ht[marshall_flat_index0];
      marshall_dictht(buf, &buf_index, ht1);
      memcpy(&TEST_data->ht[marshall_flat_index0], ht1, sizeof(*ht1));
    }
    marshall_prim(buf, &buf_index, TEST_data->rehashidx);
    marshall_prim(buf, &buf_index, TEST_data->iterators);
  }
  else
  {
    size_t size_of_element_= 0;
    marshall_prim(buf, &buf_index, size_of_element_);
  }
  *buf_index_ = buf_index;
}
int ext_return_same_from_resp(struct extension_data data)
{
  int result;
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_prim(buf, &buf_index, result);
  return result;
}
struct extension_data ext_return_same(struct extension_data data)
{
  dict* d;
  ext_return_same_from_arg(data, &d);


//TODO fix, this is bug with libcompart not shutting down with return value of 0


  int return_value;
  return_value = return_same(d);
  struct extension_data result = ext_return_same_to_resp( return_value );
  return result;
}
struct extension_data ext_return_same_to_arg(dict* d)
{
  struct extension_data data;
  char* buf = data.buf;
  size_t buf_index = 0;
  marshall_dict(buf, &buf_index, d);
  data.bufc = buf_index;
  return data;
}
struct extension_data ext_return_same_to_resp(int result)
{
  struct extension_data data;
  char* buf= data.buf;
  size_t buf_index = 0;
  marshall_prim(buf, &buf_index, result);
  data.bufc = buf_index;
  return data;
}
void ext_return_same_from_arg(struct extension_data data, dict* *d)
{
  char* buf = data.buf;
  size_t buf_index = 0;
  unmarshall_dict(buf, &buf_index, *d);
}
