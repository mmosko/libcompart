#pragma once
#include "dict.h"

#define SUCCESS 1
#define NO_SUCCESS 0

static unsigned int dictIntHash(const void* key)
{
    int i = (int*)key;
    return (unsigned int)i;
}

static int dictIntKeyCompare(void *privdata, const void *key1, const void *key2)
{
    const int i1 = (const int)key1;
    const int i2 = (const int)key2;
    return i1 == i2;
}

static void dictIntKeyDestructor(void *privdata, void *val)
{
}

/* Sets type and expires */
static dictType intDictType = {
    dictIntHash,            /* hash function */
    NULL,                      /* key dup */
    NULL,                      /* val dup */
    dictIntKeyCompare,      /* key compare */
    dictIntKeyDestructor,      /* key destructor */
    dictIntKeyDestructor,      /* val destructor */
};

#define START_INDEX 1
#define END_INDEX 8
#define VAL_OFFSET 100

dict* init_dict()
{
    dict* d = dictCreate(&intDictType, NULL);

    for(int i = START_INDEX; i < END_INDEX; i++)
    {
        dictAdd(d, (void*)i, (void*)i+VAL_OFFSET);
    }

    return d;
}

int test_dict(dict* d)
{
    dictIterator *di = dictGetIterator(d);
    dictEntry *de;

    int expected_keys[START_INDEX + END_INDEX] = { 0 };
    while((de = dictNext(di)) != NULL)
    {
        int key = (int)dictGetEntryKey(de);
        int val = (int)dictGetEntryVal(de);
        fprintf(stderr, "HMM expected %d %d\n", key, val);
        expected_keys[key] = val;
    }

    for(int i = START_INDEX; i < END_INDEX; i++)
    {
        if(expected_keys[i] != i + VAL_OFFSET)
        {
            return NO_SUCCESS;
        }
    }

    dictReleaseIterator(di);

    return SUCCESS;
}

void print_dict_elements(dict* d)
{
    dictIterator *di = dictGetIterator(d);
    dictEntry *de;

    fprintf(stderr, "ELEMENTS\n");
    while((de = dictNext(di)) != NULL)
    {
        int key = (int)dictGetEntryKey(de);
        fprintf(stderr, "TABLE: %d INDEX: %d PTR: %p KEY: %d\n", di->table, di->index, de, key);
    }
    fprintf(stderr, "SIZE %d\n", dictSize(d));
    fprintf(stderr, "\n\n");

    dictReleaseIterator(di);
}

int return_same(dict* d)
{
    return test_dict(d);
    //return SUCCESS;
}

