// RUN: printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" | python -B setup.py tagged_union_interface.h
// RUN: sed -i '/marshall_prim(buf, &buf_index, TEST_data->);/d' tagged_union_interface.h
// RUN: sed -i '/unmarshall_prim(buf, &buf_index, TEST_data_ref->);/d' tagged_union_interface.h
// RUN: %CC %C_FLAGS %s -o %t
// RUN: echo "" > %t.log
// RUN: { sudo PITCHFORK_LOG=%t.log %t || true; } | %FileCheck %s

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

#include "compart_api.h"
#include "tagged_union_interface.h"

int main()
{
    compart_init(NO_COMPARTS, comparts, default_config);
  	return_same_ext = compart_register_fn("other compartment", &ext_return_same);

  	compart_start("hello compartment");

    struct tagged_union tu;
    tu.tag = TAGGED_UNION_TAG;

	struct extension_data arg = ext_return_same_to_arg(&tu);
	int success = ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));

	printf("success: %d\n", success);
// CHECK: success: 1

	fflush(stdout);
	ext_return_same_from_resp(compart_call_fn(return_same_ext, arg));
	return 0;
}

