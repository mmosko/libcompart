#!/usr/bin/env python
# Part of the "chopchop" project at the University of Pennsylvania, USA.
# Authors: Henry Zhu. 2019.
#
#    Copyright 2021 University of Pennsylvania
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

#PROTOTYPE - Henry Zhu 6/6/19

#functions that are part of the serialization library for string (these are put on the very top of the generated c files
c_boiler_plate = '''
#ifndef _LIBCOMPART_SERIALISATION__
#define _LIBCOMPART_SERIALISATION__

void unmarshall_string_(char* buf, size_t* buf_index_, char** str);
void marshall_string(char* buf, size_t* buf_index_, char* str);

#define MARSHALL_CAT_(a, b) \
  a##b

#define MARSHALL_CAT(a, b) \
  MARSHALL_CAT_(a, b)

#define unmarshall_string(buf, buf_index, str) \
  unmarshall_string_(buf, buf_index, &str)

//only use for primitive data types
#define marshall_prim(buf, buf_index, data) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); \
memcpy(&buf[*(buf_index)], &data, MARSHALL_CAT(marshall, __LINE__)); \
*(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_prim(buf, buf_index, data) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data); \
  memcpy(&data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__)); \
  *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define marshall_size(buf, buf_index, data, size) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size; \
  memcpy(&buf[*(buf_index)], data, MARSHALL_CAT(marshall, __LINE__)); \
  *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#define unmarshall_size(buf, buf_index, data, size) \
  size_t MARSHALL_CAT(marshall, __LINE__) = sizeof(data[0]) * size; \
  data = calloc(size, sizeof(data[0])); \
  memcpy(data, &buf[*(buf_index)], MARSHALL_CAT(marshall, __LINE__)); \
  *(buf_index) += MARSHALL_CAT(marshall, __LINE__);

#endif // _LIBCOMPART_SERIALISATION__


//marshall string to [size][str]
void marshall_string(char* buf, size_t* buf_index_, char* str)
{
  size_t buf_index = *buf_index_;

  if(str)
  {
    //NULL terminate
    size_t str_length = strlen(str) + 1;
    memcpy(&buf[buf_index], &str_length, sizeof(str_length));
    buf_index += sizeof(str_length);

    memcpy(&buf[buf_index], str, str_length);
    buf_index += str_length;
  }
  else
  {
    memset(&buf[buf_index], 0, sizeof(size_t));
    buf_index += sizeof(size_t);
  }

  *buf_index_ = buf_index;
}

void unmarshall_string_(char* buf, size_t* buf_index_, char** str)
{
  *str = NULL;
  size_t buf_index = *buf_index_;

  size_t str_length = 0;
  memcpy(&str_length, &buf[buf_index], sizeof(str_length));
  buf_index += sizeof(str_length);

  if(str_length > 0)
  {
    //consider for NULL terminated
    *str = calloc(str_length, sizeof(char));
    memcpy(*str, &buf[buf_index], str_length);
    buf_index += str_length;
  }

  *buf_index_ = buf_index;
}
'''
