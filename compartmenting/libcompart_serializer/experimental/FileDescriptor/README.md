# File descriptor testing

This is an experimental test to pass file descriptors between processes

`
make
./file_descriptor
cat file_descriptor_test.txt
`

# Expected Result

file_descriptor_test.txt should have

`
Hi, I'm the server
Hi, I'm the client
`

