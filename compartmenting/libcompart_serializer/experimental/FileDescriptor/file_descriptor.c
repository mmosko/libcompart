#include <string.h>
#include <stdio.h>
#include <errno.h>

#include <fcntl.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>

// Help from:
// https://stackoverflow.com/questions/2358684/can-i-share-a-file-descriptor-to-another-process-on-linux-or-are-they-local-to-t
union {
	struct cmsghdr    cm;
	char              control[CMSG_SPACE(sizeof(int))];
} control_un;

#define MESSAGE_TO_CLIENT "Hi, I'm the server\n"
#define MESSAGE_TO_SERVER "Hi, I'm the client\n"

size_t sendfd(int socket, int fd) {
	char useless = 'l';
	struct iovec iov;
	iov.iov_base = &useless;
	iov.iov_len = sizeof(useless);

	struct msghdr msg;
	msg.msg_name = NULL;
	msg.msg_namelen = 0;

	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	struct cmsghdr  *cmptr;

	msg.msg_control = control_un.control;
	msg.msg_controllen = sizeof(control_un.control);

	cmptr = CMSG_FIRSTHDR(&msg);
	cmptr->cmsg_len = CMSG_LEN(sizeof(int));
	cmptr->cmsg_level = SOL_SOCKET;
	cmptr->cmsg_type = SCM_RIGHTS;
	*((int *) CMSG_DATA(cmptr)) = fd;

	return sendmsg(socket, &msg, 0);
}

int recvfd(int socket) {
	char buf[1];
	
	struct iovec iov;
	iov.iov_base = buf;
	iov.iov_len = sizeof(buf);
	
	struct msghdr msg;
	msg.msg_name = NULL;
	msg.msg_namelen = 0;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	struct cmsghdr  *cmptr;

	msg.msg_control = control_un.control;
	msg.msg_controllen = sizeof(control_un.control);

	cmptr = CMSG_FIRSTHDR(&msg);

	int len = recvmsg(socket, &msg, 0);

	if (len < 0) {
		perror(strerror(errno));
		return -1;
	}

	if (len == 0) {
		perror(strerror(errno));
		return -1;
	}

	return *(int*)CMSG_DATA(cmptr);
}


int main() {
	int pid;
	int pair[2];

	if (socketpair(PF_UNIX, SOCK_DGRAM, 0, pair) < 0) {
		printf("socketpair failed\n");
		return 1;
	}

	if ((pid = fork()) < 0) {
		printf("fork failed\n");
		return 1;
	}

	if (pid != 0) {
		close(pair[0]);
		
		printf("Executing parent\n");
		int fd = open("file_descriptor_test.txt", O_WRONLY | O_CREAT | O_APPEND);

		int conn = pair[1];

		write(fd, MESSAGE_TO_CLIENT, strlen(MESSAGE_TO_CLIENT));
		sendfd(conn, fd);
		close(fd);
	} else {
		close(pair[1]);
		
		printf("Executing child\n");
		
		int conn = pair[0];

		int fd = recvfd(conn);

		write(fd, MESSAGE_TO_SERVER, strlen(MESSAGE_TO_SERVER));
		close(fd);
	}

	fflush(stdout);

	return 0;
}
