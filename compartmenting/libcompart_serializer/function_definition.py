#!/usr/bin/env python

# Part of the "chopchop" project at the University of Pennsylvania, USA.
# Authors: Henry Zhu. 2019.
#
#    Copyright 2021 University of Pennsylvania
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

#PROTOTYPE - Henry Zhu 6/6/19

#wrapper for mostly holding generated text and managing it

import sys
import clang.cindex
import pprint
import re
import argparse
import doctest

import libclang_config

class BodyLine():
	def __init__(self, string, spaces = 0):
		self.string = string
		self.spaces = spaces

	def Generate(self):
		if self.string != '':
			return ' ' * self.spaces + self.string + '\n'
		return ''

class Function():
	def __init__(self, prototype = None, num_spaces = 2):
		self.prototype = prototype
		self.num_spaces = num_spaces
		self.spaces = self.num_spaces
		self.body = []
		self.extra = ''
                self.struct_argument = None
	
	def GetPrototype(self):
		return self.prototype

	def GetArgs(self):
		start = self.prototype.find("(") + 1
		end = self.prototype.find(")")
		args = self.prototype[start:end]
		args = args.split(', ')
		return args

	def SetPrototype(self, prototype):
		self.prototype = prototype

	def AddArgument(self, argument):
		end = self.prototype.find(")")
		self.prototype = self.prototype[end:] + ", " + argument + self.prototype[:end]

        def SetStructArgument(self, struct):
                self.struct_argument = struct

	def AddLine(self, line):
		if line == '}':
			self.spaces -= self.num_spaces

		self.body.append(BodyLine(line, self.spaces))

		if line == '{':
			self.spaces += self.num_spaces

	def GetBody(self):
		return self.body

	def SetBody(self, body):
		self.body = body

	def AddCLines(self, lines):
		lines = lines.split('\n')
		for line in lines:
			while len(line) > 0 and (line[0] == ' ' or line[0] == '\t'):
				line = line[1:]
			self.AddLine(line)

	def RemoveComments(self):
		for body_line in self.body:
			index = body_line.string.find(r'//')
			if index != -1:
				body_line.string = body_line.string[:body_line.string.index(r'//')]

	def GenerateBody(self):
		lines = ''
		for line in self.body:
			lines += line.Generate()
		return lines

	def Generate(self):
		function = self.prototype + '\n'
		function += '{\n'
		function += self.GenerateBody()
		function += '}\n'
		function += self.extra
		return function

