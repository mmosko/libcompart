![libcompart](libcompart_icon.png)

**libcompart** is used for the portable restructuring and execution of software as sets of isolated components.
This restructuring is done to restrict the scope of vulnerabilities to the components in which they occur, rather than immediately allow them the scope of access that would be obtained in the original (monolithic) form of the software.


## Usage, Tests and Examples

libcompart has been compiled and used on Linux, FreeBSD and macOS.
See [hello_compartment](hello_compartment/) for an example of how to use the API.
Additionally, [several examples involving third-party software](../) are provided.


## Building
Run:
```
make
```

By default, a static library is produced. For a dynamic library use:
```
LC_LIB=dynamic make
```
Then remember to set `LD_LIBRARY_PATH`.
**(This linking option doesn't yet work on macOS)**

By default, the debug version of the library is produced. For the release version use:
```
EXPERIMENT_PERFORM=YES make
```


## License
[Apache 2.0](LICENSE)


## Authors

* [Ke Zhong](https://kzhong130.github.io/)
* [Henry Zhu](https://maknee.github.io)
* [Zhilei Zheng](https://github.com/zhileiz)
* [Nik Sultana](http://www.cs.iit.edu/~nsultana1/) (lead)
