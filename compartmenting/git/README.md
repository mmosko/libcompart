Apply the [patch](patch) at revision e19713638985533ce461db072b49112da5bd2042 of the master branch at https://github.com/git/git

This was tested on Ubuntu 16.04.7 LTS.

Compile with
```
$ LIBCOMPART_PATH=.../libcompart; NO_OPENSSL=1 make CFLAGS="-g -O2 -Wall -DUSE_LIBCOMPART -I${LIBCOMPART_PATH} ${LIBCOMPART_PATH}/compart.o " -j5
```
You can append `V=1` to see verbose output from the Makefile.

Then run a git command:
```
$ ./git-status
422548 <42> initialising
422548 <80> Registered 0: 0x5b9280 at c:is_git_directory
422548 <62> starting monitor 422548 (null)
422549 <43> starting sub 422549 c:is_git_directory
422550 <11> starting 422550 c:git
422550 <81> Compart-call to 0: 0x5b9280 at c:is_git_directory
422550 <44> c:git to c:is_git_directory
422548 <45> (monitor) call to c:is_git_directory
422548 <46> (monitor) return from c:is_git_directory
422550 <81> Compart-call to 0: 0x5b9280 at c:is_git_directory
422550 <44> c:git to c:is_git_directory
422548 <45> (monitor) call to c:is_git_directory
422548 <46> (monitor) return from c:is_git_directory
422550 <81> Compart-call to 0: 0x5b9280 at c:is_git_directory
422550 <44> c:git to c:is_git_directory
422548 <45> (monitor) call to c:is_git_directory
422548 <46> (monitor) return from c:is_git_directory
422550 <81> Compart-call to 0: 0x5b9280 at c:is_git_directory
422550 <44> c:git to c:is_git_directory
422548 <45> (monitor) call to c:is_git_directory
422548 <46> (monitor) return from c:is_git_directory
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   common-main.c
        new file:   git_interface.h
        modified:   setup.c

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        .common-main.c.swp
        .setup.c.swp

422548 <50> terminated: c:git
422548 <38> communication break: between (monitor) (pid 422548) and c:git
422548 <50> terminated: c:is_git_directory
422548 <37> all children dead
```
