Apply the [patch](patch) to releng/12.1 at revision 366414.

This was tested on FreeBSD 12.1 RELEASE.

Compile with
```
/usr/src/usr.sbin/tcpdump$ sudo CFLAGS=-DUSE_LIBCOMPART LIBCOMPART_PATH=/home/nik/mnt/chopchop/compartmenting/libcompart make
```

We'll then run the test by issuing two commands, the first to produce packets and the second the run tcpdump.
This is the first command:
```
$ ping -c 5 www.example.com
PING www.example.com (93.184.216.34): 56 data bytes
64 bytes from 93.184.216.34: icmp_seq=0 ttl=63 time=3.542 ms
64 bytes from 93.184.216.34: icmp_seq=1 ttl=63 time=4.059 ms
64 bytes from 93.184.216.34: icmp_seq=2 ttl=63 time=3.905 ms
64 bytes from 93.184.216.34: icmp_seq=3 ttl=63 time=4.036 ms
64 bytes from 93.184.216.34: icmp_seq=4 ttl=63 time=3.740 ms

--- www.example.com ping statistics ---
5 packets transmitted, 5 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 3.542/3.856/4.059/0.194 ms
```

Prior to running the first command, you need to start tcpdump. The unmodified tcpdump produces output similar to that below:
```
$ sudo /usr/obj/home/nik/mnt/src/amd64.amd64/usr.sbin/tcpdump/tcpdump/tcpdump icmp
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on vtnet0, link-type EN10MB (Ethernet), capture size 262144 bytes
21:16:53.224352 IP 10.0.2.15 > 93.184.216.34: ICMP echo request, id 23858, seq 0, length 64
21:16:53.228056 IP 93.184.216.34 > 10.0.2.15: ICMP echo reply, id 23858, seq 0, length 64
21:16:54.250438 IP 10.0.2.15 > 93.184.216.34: ICMP echo request, id 23858, seq 1, length 64
21:16:54.254060 IP 93.184.216.34 > 10.0.2.15: ICMP echo reply, id 23858, seq 1, length 64
21:16:55.322642 IP 10.0.2.15 > 93.184.216.34: ICMP echo request, id 23858, seq 2, length 64
21:16:55.330205 IP 93.184.216.34 > 10.0.2.15: ICMP echo reply, id 23858, seq 2, length 64
21:16:56.360325 IP 10.0.2.15 > 93.184.216.34: ICMP echo request, id 23858, seq 3, length 64
21:16:56.363999 IP 93.184.216.34 > 10.0.2.15: ICMP echo reply, id 23858, seq 3, length 64
21:16:57.373277 IP 10.0.2.15 > 93.184.216.34: ICMP echo request, id 23858, seq 4, length 64
21:16:57.377179 IP 93.184.216.34 > 10.0.2.15: ICMP echo reply, id 23858, seq 4, length 64
^C
10 packets captured
55 packets received by filter
0 packets dropped by kernel
```

The modified tcpdump produces output like the following:
```
/usr/src/usr.sbin/tcpdump$ sudo /usr/obj/home/nik/mnt/src/amd64.amd64/usr.sbin/tcpdump/tcpdump/tcpdump icmp
14054 <42> initialising
14054 <80> Registered 0: 0x2bcf80 at c:icmp
14058 <43> starting sub 14058 c:icmp
14054 <62> starting monitor 14054 (null)
14059 <11> starting 14059 c:tcpdump
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on vtnet0, link-type EN10MB (Ethernet), capture size 262144 bytes
14059 <81> Compart-call to 0: 0x2bcf80 at c:icmp
14059 <44> c:tcpdump to c:icmp
14054 <45> (monitor) call to c:icmp
ICMP echo request, id 60470, seq 0, length 64
14054 <46> (monitor) return from c:icmp
21:44:29.361678 IP 10.0.2.15 > 93.184.216.34:
14059 <81> Compart-call to 0: 0x2bcf80 at c:icmp
14059 <44> c:tcpdump to c:icmp
14054 <45> (monitor) call to c:icmp
ICMP echo reply, id 60470, seq 0, length 64
14054 <46> (monitor) return from c:icmp
21:44:29.365559 IP 93.184.216.34 > 10.0.2.15:
14059 <81> Compart-call to 0: 0x2bcf80 at c:icmp
14059 <44> c:tcpdump to c:icmp
14054 <45> (monitor) call to c:icmp
ICMP echo request, id 60470, seq 1, length 64
14054 <46> (monitor) return from c:icmp
21:44:30.388104 IP 10.0.2.15 > 93.184.216.34:
14059 <81> Compart-call to 0: 0x2bcf80 at c:icmp
14059 <44> c:tcpdump to c:icmp
14054 <45> (monitor) call to c:icmp
ICMP echo reply, id 60470, seq 1, length 64
14054 <46> (monitor) return from c:icmp
21:44:30.391707 IP 93.184.216.34 > 10.0.2.15:
14059 <81> Compart-call to 0: 0x2bcf80 at c:icmp
14059 <44> c:tcpdump to c:icmp
14054 <45> (monitor) call to c:icmp
ICMP echo request, id 60470, seq 2, length 64
14054 <46> (monitor) return from c:icmp
21:44:31.456915 IP 10.0.2.15 > 93.184.216.34:
14059 <81> Compart-call to 0: 0x2bcf80 at c:icmp
14059 <44> c:tcpdump to c:icmp
14054 <45> (monitor) call to c:icmp
ICMP echo reply, id 60470, seq 2, length 64
14054 <46> (monitor) return from c:icmp
21:44:31.460562 IP 93.184.216.34 > 10.0.2.15:
14059 <81> Compart-call to 0: 0x2bcf80 at c:icmp
14059 <44> c:tcpdump to c:icmp
14054 <45> (monitor) call to c:icmp
ICMP echo request, id 60470, seq 3, length 64
14054 <46> (monitor) return from c:icmp
21:44:32.468596 IP 10.0.2.15 > 93.184.216.34:
14059 <81> Compart-call to 0: 0x2bcf80 at c:icmp
14059 <44> c:tcpdump to c:icmp
14054 <45> (monitor) call to c:icmp
ICMP echo reply, id 60470, seq 3, length 64
14054 <46> (monitor) return from c:icmp
21:44:32.472318 IP 93.184.216.34 > 10.0.2.15:
14059 <81> Compart-call to 0: 0x2bcf80 at c:icmp
14059 <44> c:tcpdump to c:icmp
14054 <45> (monitor) call to c:icmp
ICMP echo request, id 60470, seq 4, length 64
14054 <46> (monitor) return from c:icmp
21:44:33.477213 IP 10.0.2.15 > 93.184.216.34:
14059 <81> Compart-call to 0: 0x2bcf80 at c:icmp
14059 <44> c:tcpdump to c:icmp
14054 <45> (monitor) call to c:icmp
ICMP echo reply, id 60470, seq 4, length 64
14054 <46> (monitor) return from c:icmp
21:44:33.480679 IP 93.184.216.34 > 10.0.2.15:
^C
10 packets captured
159 packets received by filter
0 packets dropped by kernel
```
