Apply the [patch](patch) at revision 166566779b2e1ea5c09094d4c29cb11d1f945ad4 of the master branch at https://gitlab.gnome.org/GNOME/evince

This was tested on Ubuntu 16.04.7 LTS.

Compile libcompart as follows:
```
$ make clean
$ LC_LIB=dynamic CFLAGS="-DMAX_COMPART_REGS=30 -DEXT_ARG_BUF_SIZE=200000" make
$ CFLAGS="-DMAX_COMPART_REGS=30 -DEXT_ARG_BUF_SIZE=200000" make
```

Prepare evince for building:
```
$ ./autogen.sh  --disable-nautilus --prefix=...
```

Compile it with:
```
$ export LIBCOMPART_PATH=.../libcompart
$ make clean
$ make CFLAGS="-DUSE_LIBCOMPART -I${LIBCOMPART_PATH} -DEXT_ARG_BUF_SIZE=200000" LDFLAGS=" ${LIBCOMPART_PATH}/libcompart.so " -j12
$ make install
```
You can append `V=1` to see verbose output from the Makefile.

Then start evince and open a .ps file
```
$ ./shell/evince
176783 <42> initialising
176783 <80> Registered 0: 0x41c970 at c:spectre
176783 <80> Registered 1: 0x4190a0 at c:spectre
176783 <80> Registered 2: 0x41c920 at c:spectre
176783 <80> Registered 3: 0x41db80 at c:spectre
176783 <80> Registered 4: 0x41b620 at c:spectre
176783 <80> Registered 5: 0x419dc0 at c:spectre
176783 <80> Registered 6: 0x419380 at c:spectre
176783 <80> Registered 7: 0x41ac60 at c:spectre
176783 <80> Registered 8: 0x41a140 at c:spectre
176783 <80> Registered 9: 0x419a30 at c:spectre
176783 <80> Registered 10: 0x41c530 at c:spectre
176783 <80> Registered 11: 0x41a2e0 at c:spectre
176783 <80> Registered 12: 0x41c7c0 at c:spectre
176783 <80> Registered 13: 0x41c370 at c:spectre
176783 <80> Registered 14: 0x418ea0 at c:spectre
176783 <80> Registered 15: 0x41a610 at c:spectre
176783 <80> Registered 16: 0x41c890 at c:spectre
176783 <80> Registered 17: 0x41c460 at c:spectre
176783 <80> Registered 18: 0x41cd10 at c:spectre
176783 <80> Registered 19: 0x41ae90 at c:spectre
176797 <43> starting sub 176797 c:spectre
176783 <62> starting monitor 176783 (null)
176798 <11> starting 176798 c:evince
Xlib:  extension "GLX" missing on display ":1".
176798 <81> Compart-call to 2: 0x41c920 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 3: 0x41db80 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 4: 0x41b620 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 11: 0x41a2e0 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 9: 0x419a30 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 8: 0x41a140 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 6: 0x419380 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 12: 0x41c7c0 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 13: 0x41c370 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 5: 0x419dc0 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 5: 0x419dc0 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 9: 0x419a30 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 8: 0x41a140 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 7: 0x41ac60 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 10: 0x41c530 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 6: 0x419380 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 9: 0x419a30 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 8: 0x41a140 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 7: 0x41ac60 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 15: 0x41a610 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 16: 0x41c890 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 17: 0x41c460 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 19: 0x41ae90 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 18: 0x41cd10 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 6: 0x419380 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre

(lt-evince:176798): Gtk-WARNING **: Error loading image 'resource:///org/gnome/evince/shell/ui/thumbnail-frame.png': The resource at '/org/gnome/evince/shell/ui/thumbnail-frame.png' does not exist
176798 <81> Compart-call to 0: 0x41c970 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre

(lt-evince:176798): Gtk-WARNING **: Error loading image 'resource:///org/gnome/evince/shell/ui/thumbnail-frame.png': The resource at '/org/gnome/evince/shell/ui/thumbnail-frame.png' does not exist
176798 <81> Compart-call to 2: 0x41c920 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 3: 0x41db80 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 4: 0x41b620 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 11: 0x41a2e0 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 9: 0x419a30 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 8: 0x41a140 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 6: 0x419380 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 12: 0x41c7c0 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 13: 0x41c370 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 5: 0x419dc0 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 5: 0x419dc0 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 9: 0x419a30 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 8: 0x41a140 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 7: 0x41ac60 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 10: 0x41c530 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 6: 0x419380 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 9: 0x419a30 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 6: 0x419380 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 9: 0x419a30 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 8: 0x41a140 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 7: 0x41ac60 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 15: 0x41a610 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 16: 0x41c890 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 17: 0x41c460 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 19: 0x41ae90 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 18: 0x41cd10 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 6: 0x419380 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 9: 0x419a30 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 8: 0x41a140 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 7: 0x41ac60 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 15: 0x41a610 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 16: 0x41c890 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 17: 0x41c460 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 19: 0x41ae90 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 18: 0x41cd10 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176798 <81> Compart-call to 6: 0x419380 at c:spectre
176798 <44> c:evince to c:spectre
176783 <45> (monitor) call to c:spectre
176783 <46> (monitor) return from c:spectre
176783 <50> terminated: c:evince
176783 <38> communication break: between (monitor) (pid 176783) and c:evince
176783 <50> terminated: c:spectre
176783 <37> all children dead
```
