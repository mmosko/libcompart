Apply the [patch](patch) at revision c0b15329e304013ac222a4cbba2458c407525553 of the master branch at github.com/ioquake/ioq3.git

This was tested on macOS 10.15.2.

Compile with
```
$ LIBCOMPART_PATH=.../libcompart; CFLAGS="-DUSE_LIBCOMPART -I${LIBCOMPART_PATH}" LDFLAGS=${LIBCOMPART_PATH}/compart.o make USE_AUTOUPDATER=1
```
You can append `V=1` to see verbose output from the Makefile.

Then run with
```
ioq3 $ ./build/release-darwin-x86_64/ioquake3.x86_64
16264 <42> initialising
16264 <80> Registered 0: 0x10e18c4a0 at c:autoupdate
16264 <62> starting monitor 16264 (null)
16265 <43> starting sub 16265 c:autoupdate
16266 <11> starting 16266 c:ioquake3
16266 <81> Compart-call to 0: 0x10e18c4a0 at c:autoupdate
16266 <44> c:ioquake3 to c:autoupdate
16264 <45> (monitor) call to c:autoupdate
16264 <46> (monitor) return from c:autoupdate
ioq3 1.36_GIT_c0b15329-2020-08-29 macosx-x86_64 Oct  5 2020
SSE instruction set enabled
----- FS_Startup -----
...
----- Client Shutdown (Client quit) -----
RE_Shutdown( 1 )
Hunk_Clear: reset the hunk ok
OpenAL capture device closed.
-----------------------
16264 <50> terminated: c:ioquake3
16264 <38> communication break: between (monitor) (pid 16264) and c:ioquake3
16264 <50> terminated: c:autoupdate
16264 <37> all children dead
```
