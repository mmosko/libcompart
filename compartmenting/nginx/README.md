Apply the [patch](patch) at revision 1e92a0a4cef98902aed35d7b402a6a402951aba4 of the master branch at https://github.com/nginx/nginx

This was tested on Ubuntu 16.04.7 LTS.

Compile with
```
$ export LIBCOMPART_PATH=...; make CFLAGS="-pipe -O -W -Wall -Wpointer-arith -Wno-unused-parameter -Werror -g -DUSE_LIBCOMPART -I${LIBCOMPART_PATH}" -j12
```

Configure (prefix/conf/nginx.conf):
* Added "daemon off;"
* Added http->server added "listen 8012;"

Run server by calling `$ objs/nginx` and test it using a browser.
You should expect this console output:
```
199666 <42> initialising
199666 <80> Registered 0: 0x44713c at c:parse_req
199666 <62> starting monitor 199666 (null)
199667 <43> starting sub 199667 c:parse_req
199668 <11> starting 199668 c:nginx
199666 <45> (monitor) call to c:parse_req
199666 <46> (monitor) return from c:parse_req
```
and this is in the error.log (which captures stderr from libcompart's debug output):
```
199669 <81> Compart-call to 0: 0x44713c at c:parse_req
199669 <44> c:nginx to c:parse_req
```
