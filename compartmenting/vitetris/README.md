This was tested on Ubuntu 16.04.7 LTS and:
- ncurses at revision a7e05fb9806cc1255b6ba4fb29e15d337f35b2ad of the master branch at https://github.com/mirror/ncurses
- vitetris at revision 773418418f566243d5552061d2252253f4fc02b8 of the master branch at https://github.com/vicgeralds/vitetris

Apply the patches:
- [libcompart building](patch_libcompart)
- [vitetris](patch_vitetris)


## Compiling
Configure the compilation of vitetris with:
```
./configure curses=-lncurses
```
You might need to manually edit the resulting `config.mk` to ensure the following:
```
TWOPLAYER = y
JOYSTICK = y
NETWORK = y
TTY_SOCKET = y
TERM_RESIZING = y
MENU = y
BLOCKSTYLES = y
#INPUT_SYS
CURSES = y
```
Then compile with:
```
vitetris/src$ make clean && make
```

## Running
```
~/libcompart_improvements/vitetris/src$ PITCHFORK_LOG=libcompart_log ./tetris -listen 20010
```

This logs to `libcompart_log` to avoids interfering with the UI. Inspecting the log shows:
```
215022 <42> initialising
215022 <80> Registered 0: 0x417f7a at c:listen
215022 <80> Registered 1: 0x4186bb at c:recvplayer
215023 <43> starting sub 215023 c:listen
215022 <62> starting monitor 215022 (null)
215024 <43> starting sub 215024 c:recvplayer
215025 <11> starting 215025 c:main
215025 <81> Compart-call to 0: 0x417f7a at c:listen
215025 <44> c:main to c:listen
215022 <45> (monitor) call to c:listen
215022 <46> (monitor) return from c:listen
215025 <44> c:main terminating c:listen
215022 <74> (monitor) notified of compart_ending c:listen
```
Meanwhile the terminal output shows:
```
Listening for connections on port 20010...
Press ENTER to continue
```
You can inspect it at runtime using a command like `rm ~/libcompart_improvements/vitetris//libcompart_log ; ( touch ~/libcompart_improvements/vitetris/libcompart_log && tail -f ~/libcompart_improvements/vitetris/libcompart_log )`

Upon hitting ENTER, you'll enter a menu to configure your client, and will be told "Waiting for opponent...". The game will start once another vitetris instance joins you. Until you're joined by another player, the log shows now the interactions between compartments, since there's no connection-related activity.
As soon as another player joins, you'll be shown their name and the log will start showing connection activity:
```
215025 <81> Compart-call to 1: 0x4186bb at c:recvplayer
215025 <44> c:main to c:recvplayer
215022 <45> (monitor) call to c:recvplayer
215022 <46> (monitor) return from c:recvplayer
...
```

To simulate another player joining, start another instance of `./tetris` from another shell:
```
PITCHFORK_LOG=libcompart_log_client ./tetris
```
NOTE that we're logging to a different file, to avoid interfering with the first instance.
Then choose "Netplay" in the menu and use the same port number as above.
The log for this client will look like:
```
215046 <42> initialising
215046 <80> Registered 0: 0x417f7a at c:listen
215046 <80> Registered 1: 0x4186bb at c:recvplayer
215046 <62> starting monitor 215046 (null)
215047 <43> starting sub 215047 c:listen
215048 <43> starting sub 215048 c:recvplayer
215049 <11> starting 215049 c:main
215049 <81> Compart-call to 1: 0x4186bb at c:recvplayer
215049 <44> c:main to c:recvplayer
215046 <45> (monitor) call to c:recvplayer
215046 <46> (monitor) return from c:recvplayer
215049 <81> Compart-call to 1: 0x4186bb at c:recvplayer
215049 <44> c:main to c:recvplayer
215046 <45> (monitor) call to c:recvplayer
215046 <46> (monitor) return from c:recvplayer
...
```

From each instance you can move the tetris tiles for that player.

When one player quits, the game ends.
When a player terminates their instances, you'll see the usual graceful shutdown. For example, you'll see this for the first instance:
```
215022 <50> terminated: c:main
215022 <38> communication break: between (monitor) (pid 215022) and c:main
215022 <50> terminated: c:listen
215022 <50> terminated: c:recvplayer
215022 <37> all children dead
```
