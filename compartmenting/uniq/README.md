Apply the [patch](patch) to releng/12.1 at revision 366414.

This was tested on FreeBSD 12.1 RELEASE.

Compile with
```
$ LIBCOMPART_PATH=/home/nik/mnt/chopchop/compartmenting/libcompart CFLAGS=-DUSE_LIBCOMPART make
```

Then run with
```
$ for X in `seq 5`; do echo $X; echo $X; done | /usr/obj/amd64.amd64/usr.bin/uniq/uniq
6147 <42> initialising
6147 <62> starting monitor 6147 (null)
6149 <11> starting 6149 main compartment
1
2
3
4
5
6147 <50> terminated: main compartment
6147 <37> all children dead
```
